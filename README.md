## Как запускать

Легкий путь: запустить [этот блокнот на Колабе](https://colab.research.google.com/github/Tony607/object_detection_demo/blob/master/tensorflow_object_detection_training_colab.ipynb).

```
https://github.com/Tony607/object_detection_demo
```
### Install required libraries
`pip3 install -r requirements.txt`
`pip3 install -r tensorflow==1.15`


### Шаг 1: Разметить датасет с картинками
- Сохранить изображения в формате jpg(jpeg) в директорию `./data/raw`.
- Ресайзнуть изображения до единого формата `(800, 600)` с помощью команды
```
python resize_images.py --raw-dir ./data/raw --save-dir ./data/images --ext jpg --target-size "(800, 600)"
```
Расположение изображений с измененным размером в директории `./data/images/`
- Разделить изображения по директориям, `./data/images/train` и `./data/images/test`

- Разметить с помощью приложения [labelImg](https://tzutalin.github.io/labelImg/), которое генерирует `xml` файлы внутри `./data/images/train` и `./data/images/test` директорий.

*Подсказки: можно использовать (`w`: создать область, и т.д.) для ускорения разметки.*

- Закоммитить и запушить (`./data/images/train` и `./data/images/test`) в репозиторий.


### Шаг 2: Открыть [Блокнот на Колабе](https://colab.research.google.com/github/Tony607/object_detection_demo/blob/master/tensorflow_object_detection_training_colab.ipynb)
- Replace the repository's url to yours and run it.


## Как получить вывод модели

Требуется:
- `frozen_inference_graph.pb` Замороженная модель обнаружения объектов TensorFlow, загруженная из Colab после обучения.
- `label_map.pbtxt` Файл, используемый для сопоставления правильного имени для индекса прогнозируемого класса, загруженного из Colab после обучения.


## Запуск теста

Примеры

Тестирование SSD mobileNet V2 на графическом процессоре с квантованными весами FP16.
```
cd ./deploy
python openvino_inference_benchmark.py\
     --model-dir ./models/ssd_mobilenet_v2_custom_trained/FP16\
     --device GPU\
     --data-type FP16\
     --img ../test/E00GWDMXsAA1BjP.jpg
```
Тест TF на CPU
```
python local_inference_test.py\
     --model ./models/frozen_inference_graph.pb\
     --img ./test/E00GWDMXsAA1BjP.jpg\
     --cpu
```
